<!DOCTYPE html>
<html lang="de">
    <?php
       //define flexible base
       $baseurl = rex_yrewrite::getCurrentDomain()->getUrl();
       if (!rex_addon::get('yrewrite')->isAvailable()) {
           $baseurl = rex::getServer();
       }

    ?>
	<head>
		<base href="<?=$baseurl ?>">

		<meta charset="utf-8">
    <meta name="referrer" content="no-referrer">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

		<?php
            $seo = new rex_yrewrite_seo();
            echo $seo->getTitleTag().PHP_EOL;
            echo $seo->getDescriptionTag().PHP_EOL;
            echo $seo->getRobotsTag().PHP_EOL;
            echo $seo->getHreflangTags().PHP_EOL;
            echo $seo->getCanonicalUrlTag().PHP_EOL;
        ?>


		<?php
            //nachschauen ob im globals-asset-folder ein less/scss main_variables.scss vorhanden ist,
            //das die basic-variablen (font, farben, etc) vorgibt, dann erst die main.scss laden
            //ansonsten fallback auf defaults in rex_bp/_defaults/main_variables.scss
        ?>

		<link rel="shortcut icon" type="image/ico" href="/assets/rex_bp/<?= rex_config::get('gloebals', 'rex-bolierplate')[rex_yrewrite::getCurrentDomain()->getId()]['assetfolder'] ?>/img/favicon.ico">
		<link rel="stylesheet" type="text/css" href="<?php

        $minify = new minify();
        foreach ((glob(rex_path::assets()."rex_bp/".(rex_config::get('gloebals', 'rex-bolierplate')[rex_yrewrite::getCurrentDomain()->getId()]['assetfolder'])."/css/vendor/*.*css")) as $css) {
            $minify->addFile("/".str_replace(rex_path::base(), "", $css), $set = rex_config::get('gloebals', 'rex-bolierplate')[rex_yrewrite::getCurrentDomain()->getId()]['assetfolder']);
        };
        foreach ((glob(rex_path::assets()."rex_bp/".(rex_config::get('gloebals', 'rex-bolierplate')[rex_yrewrite::getCurrentDomain()->getId()]['assetfolder'])."/css/*.*css")) as $css) {
            $minify->addFile("/".str_replace(rex_path::base(), "", $css), $set = rex_config::get('gloebals', 'rex-bolierplate')[rex_yrewrite::getCurrentDomain()->getId()]['assetfolder']);
        };
        echo rex_path::absolute(
            $minify->minify($type = 'css', $set = rex_config::get('gloebals', 'rex-bolierplate')[rex_yrewrite::getCurrentDomain()->getId()]['assetfolder'])
        );
/*
 *	<link rel="stylesheet" type="text/css" href="assets/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="assets/css/fonts.css">
    <link rel="stylesheet" type="text/css" href="assets/css/grid12.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="assets/css/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="assets/css/swipebox.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/bundled.default.css">

 */

        ?>">

		<script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
        <script src="assets/js/vendor/lazysizes.min.js"></script>
	</head>

<?php


//TEMPLATE 3

$depth = rex_config::get('gloebals', 'rex-bolierplate')[rex_yrewrite::getCurrentDomain()->getId()]['navi-depth']; # $rxb_config['navi-depth'];
$navi_root_id = ((rex_config::get('gloebals', 'rex-bolierplate')[rex_yrewrite::getCurrentDomain()->getId()]['navi-root-id']!== false) ? 0 :  rex_article::getSiteStartArticleId()) ;

// register navigation
$nav = rex_navigation::factory();
//$nav->setUlClass('link-list');
$nav->setLinkClasses(array('link'));
//$nav->setCurrentAClass('link-active');
$navMain = $nav->get($navi_root_id, $depth, $cur_cat=true, $ignore_offlines=true);

// Breadcrumb
$sBreadcrumb = '';
if (!rex_article::getCurrent()->isSiteStartArticle()) {
    $nav = rex_navigation::factory();
    $sBreadcrumb = '
		<div class="mod-breadcrumb">
			<div class="container">
				<div class="row">
					<div class="breadcrumb col-12">
						<div class="breadcrumb-title typo-breadcrumb-title">'.rex_article::getCurrent()->getName().'</div>
						<nav>
							'.$nav->getBreadcrumb('Home', true).'
						</nav>
					</div>
				</div>
			</div>
		</div>
	';
}

?>

<body>

	<header>
		<?php
            //evtl via addon ein alternatives template
   //TEMPLATE 6
?>
<div class="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="logo">
					<a href="<?= $baseurl?>">
						<h1 class="typo-logo">
							<?= rex_config::get('gloebals', 'rex-bolierplate')[rex_yrewrite::getCurrentDomain()->getId()]['site-title'] # $rxb_config['site-title'] # rex_config::get('gloebals', $key="site-title",  $default="Name")?>
						</h1>
						<p><?= rex_config::get('gloebals', 'rex-bolierplate')[rex_yrewrite::getCurrentDomain()->getId()]['site-owner'] # $rxb_config['site-owner'] # rex_config::get('gloebals', $key="site-owner",  $default="Name")?></p>
					</a>
				</div>

				<div class="nav-main">
					<nav><?php #dump($navMain);die;
echo($navMain) ; ?></nav>
				</div>

				<div class="button-nav">
					<div class="icon icon-menu">
						<div class="line"></div>
						<div class="line"></div>
						<div class="line"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 </header>

	<main>
		<?php echo $sBreadcrumb ?>
		REX_ARTICLE[]
	</main>

	<footer>
     <?php
       //TEMPLATE 7

       //dynamic loading with footer module from article
       $footer_id = rex_config::get('gloebals', 'rex-bolierplate')[rex_yrewrite::getCurrentDomain()->getId()]['footer-id'];
       if (isset($footer_id)) {
           $article = new rex_article_content($footer_id);
           echo $article->getArticle('1');
       } else {
                 echo "<span>&copy; ".date("Y") . " " .rex_config::get('gloebals', 'rex-bolierplate')[rex_yrewrite::getCurrentDomain()->getId()]['site-owner']." ".rex_config::get('gloebals', 'rex-bolierplate')[rex_yrewrite::getCurrentDomain()->getId()]['site-title']. "</span>";
             }
     ?>
	</footer>

<?php
        foreach ((glob(rex_path::assets()."rex_bp/".(rex_config::get('gloebals', 'rex-bolierplate')[rex_yrewrite::getCurrentDomain()->getId()]['assetfolder'])."/js/vendor/*.js")) as $js) {
            $minify->addFile("/".str_replace(rex_path::base(), "", $js), $set = rex_config::get('gloebals', 'rex-bolierplate')[rex_yrewrite::getCurrentDomain()->getId()]['assetfolder']."js");
        };
        foreach ((glob(rex_path::assets()."rex_bp/".(rex_config::get('gloebals', 'rex-bolierplate')[rex_yrewrite::getCurrentDomain()->getId()]['assetfolder'])."/js/*.js")) as $js) {
            $minify->addFile("/".str_replace(rex_path::base(), "", $js), $set = rex_config::get('gloebals', 'rex-bolierplate')[rex_yrewrite::getCurrentDomain()->getId()]['assetfolder']."js");
        };

        echo '<script src="';
        echo rex_path::absolute(
            $minify->minify($type = 'js', $set = rex_config::get('gloebals', 'rex-bolierplate')[rex_yrewrite::getCurrentDomain()->getId()]['assetfolder']."js")
        );
        #echo rex_path::absolute($baseurl.$minify->minify($type = 'js', $set = rex_config::get('gloebals', 'rex-bolierplate')[rex_yrewrite::getCurrentDomain()->getId()]['assetfolder']."-js") );
        echo '"></script>';


?>


</body>

</html>
