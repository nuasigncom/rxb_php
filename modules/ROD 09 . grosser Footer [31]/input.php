<!-- *******************************************************
FOOTER KLEIN
******************************************************** -->

<fieldset class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label">Hintergrund</label>
		<div class="col-sm-10">
			<?php
			$options = array(
				'black'=>'Dunkelgrau',
				'gray'=>'Hellgrau',
				'white'=>'Weiß'
			);
			?>
			<div class="rex-select-style">
				<select name="REX_INPUT_VALUE[11]" class="form-control">
					<?php foreach ($options as $k=>$v) : ?>
						<option value="<?php echo $k; ?>"<?php if ($k == "REX_VALUE[11]") echo ' selected="selected"' ?>><?php echo $v; ?></option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
	</div>

	<br>
	<div class="panel panel-edit">
		<header class="panel-heading">
				Animation
		</header>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label" for="headline">Links in Hidden-Bereich setzen (Original-Setting aus redaxo-demo)</label>
		<div class="col-sm-10">
			<div class="checkbox">
			  <label>
			    <input type="checkbox" name="REX_INPUT_VALUE[20]" value="1"<?php if ("REX_VALUE[20]" == '1') echo ' checked'; ?>>
			    	ja
			  </label>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label" for="headline">Einblenden?</label>
		<div class="col-sm-10">
			<div class="checkbox">
			  <label>
			    <input type="checkbox" name="REX_INPUT_VALUE[19]" value="1"<?php if ("REX_VALUE[19]" == '1') echo ' checked'; ?>>
			    	ja
			  </label>
			</div>
		</div>
	</div>

	<br>
	<div class="panel panel-edit">
		<header class="panel-heading">
				Links
		</header>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label" for="markitup_1">Überschrift Linkliste 1 (Der Content der Liste kann "Hidden" dargestellt werden)</label>
		<div class="col-sm-10">
			<textarea cols="1" rows="6" class="form-control markitupEditor-full" id="markitup_1" name="REX_INPUT_VALUE[1]">REX_VALUE[1]</textarea>
		</div>
	</div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Links</label>
        <div class="col-sm-10">
            REX_LINKLIST[id="1" widget="1"]
        </div>
    </div>

    <div class="form-group">
		<label class="col-sm-2 control-label" for="markitup_2">Überschrift Linkliste 2</label>
		<div class="col-sm-10">
			<textarea cols="1" rows="6" class="form-control markitupEditor-full" id="markitup_2" name="REX_INPUT_VALUE[2]">REX_VALUE[2]</textarea>
		</div>
	</div>

	<br>
	<div class="panel panel-edit">
		<header class="panel-heading">
			Beschreibung
		</header>
	</div>

    <div class="form-group">
        <label class="col-sm-2 control-label" for="markitup_4">Text</label>
        <div class="col-sm-10">
            <textarea cols="1" rows="6" class="form-control markitupEditor-textile_full" id="markitup_4" name="REX_INPUT_VALUE[4]">REX_VALUE[4]</textarea>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label" for="headline">Überschrift Social Links</label>
        <div class="col-sm-10">
            <input class="form-control" id="headline" type="text" name="REX_INPUT_VALUE[3]" value="REX_VALUE[3]" />
        </div>
    </div>

    <br>
    <div class="panel panel-edit">
        <header class="panel-heading">
            Unterer Footer
        </header>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="headline">Text</label>
        <div class="col-sm-10">
            <input class="form-control" id="headline" type="text" name="REX_INPUT_VALUE[5]" value="REX_VALUE[5]" />

			<br>
			<section class="rex-page-section">
				<div class="panel panel-default">

					<header class="panel-heading collapsed" data-toggle="collapse" data-target="#collapse-REX_SLICE_ID" aria-expanded="false">
						<div class="panel-title"><i class="rex-icon rex-icon-info"></i> Hinweis</div>
					</header>

					<div id="collapse-REX_SLICE_ID" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">

						<div class="panel-body" style="background: #f3f6fb;">
							<ul>
								<li>Im Copyright-Feld kann das jeweils aktuelle Jahr ausgegeben werden mit:<br><br><pre>###year###</pre></li>
							</ul>
						</div>
					</div>
				</div>
			</section>

        </div>
    </div>

</fieldset>
