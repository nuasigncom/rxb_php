<?php
$animate = '';
if ("REX_VALUE[19]" == '1') {
    $animate = ' animate';
}
if ("REX_VALUE[20]" == '1') {
    $hidden_link = true;
}
?>
<footer class="footer footer-big footer-color-REX_VALUE[11]" data-color="REX_VALUE[11]">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3">
                <div class="info<?php echo $animate; ?>">
					<?php
                    $link_output = '';
                    if ('REX_VALUE[id=1 isset=1]') {
                        $link_output .= markitup::parseOutput('textile', 'REX_VALUE[id=1 output="html" prefix="<p class=header-section>" suffix="</p>"]');
                    }

                    if ('REX_LINKLIST[1]' != '') {
                        $hidden_content = '';

                        $link_output .= '<p>';
                        $linklist = explode(',', 'REX_LINKLIST[1]');
                        foreach ($linklist as $link) {
                            $art = rex_article::get($link);
                            if (is_object($art)) {
                                $art_name = $art->getValue('name');
                                if (isset($hidden_link)) {
                                    $link_output .= '
                                <a class="container-toggle" data-container="'.$art->getValue('id').'" href="#">'.$art_name.'</a><br>';

                                    $hidden_content .= '
                                <div class="hidden_content" data-container="'.$art->getValue('id').'">';

                                    $hidden_article_content = new rex_article_content($art->getId());
                                    $hidden_content .= $hidden_article_content->getArticle();

                                    $hidden_content .= '
                                  <div class="close-block" id="close-button_'.$art->getValue('id').'">
                                      <a data-container="'.$art->getValue('id').'" class="container-toggle" href="#">
                                          <span class="icon-close"><i class="pe-7s-close-circle"></i></span>
                                          </a>
                                  </div>
            			              </div>';
                                }
                                else {
                                  $link_output .= '
                              <a class="no-toggle" href="'.rex_getUrl($art->getValue("id")).'">'.$art_name.'</a><br>';
                                }
                            }
                        }
                        $link_output .= '</p>';
                    }

                    echo $link_output;
                    ?>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="info<?php echo $animate; ?>">
                <?php
                    if ('REX_VALUE[id=1 isset=2]') {
                        echo markitup::parseOutput('textile', 'REX_VALUE[id=2 output="html" prefix="<p class=header-section>" suffix="</p>"]');
                    }
                ?>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="info<?php echo $animate; ?>">
					<?php
                    if ('REX_VALUE[id=4 isset=2]') {
                        echo markitup::parseOutput('textile', 'REX_VALUE[id=4 output="html"]');
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-2 col-md-offset-1 col-sm-3">
                <div class="info animate">
                    <?php
                    if ("REX_VALUE[3]" != '') {
                        echo markitup::parseOutput('textile', 'REX_VALUE[id=3 output="html" prefix="<p class=header-section>" suffix="</p>"]');
                    }
                    if (rex::getProperty('facebook_link') != '' || rex::getProperty('twitter_link') != '') {
                        echo '<nav><ul>';
                    }
                        if (rex::getProperty('facebook_link') != '') {
                            echo '
                            <li><a class="btn btn-social btn-facebook btn-simple" href="'.rex::getProperty('facebook_link').'">
                                <i class="fa fa-facebook-square"></i> Facebook
                            </a></li>';
                        }
                        if (rex::getProperty('twitter_link') != '') {
                            echo '
                            <li><a class="btn btn-social btn-twitter btn-simple" href="'.rex::getProperty('twitter_link').'">
                                <i class="fa fa-twitter"></i> Twitter
                            </a></li>';
                        }
                    if (rex::getProperty('facebook_link') != '' || rex::getProperty('twitter_link') != '') {
                        echo '</ul></nav>';
                    }
                    ?>
                </div>
            </div>
        </div>
        <hr>
        <?php
        if ("REX_VALUE[5]" != '') {
            echo '
			<div class="copyright">
			    '.str_replace('###year###', date('Y'), markitup::parseOutput('textile', 'REX_VALUE[id=5 output="html"]')).'
			</div>';
        }
        ?>
    </div>
</footer>

<?php
if (!rex::isBackend()) {
            echo $hidden_content;
        }
?>
