<?php
if (!rex::isBackend()):
      echo "REX_VALUE[id=1 output=php]";
else:
      ?>
      <div>
            <textarea id="quellcodeREX_SLICE_ID" name="quellcodeREX_SLICE_ID"  class="form-control" readonly>REX_VALUE[id=1 output=html]</textarea>
            <script>
                  $(function () {
                        CodeMirror.fromTextArea(document.getElementById('quellcodeREX_SLICE_ID'), {
                              mode: "javascript",
                              theme: "default",
                              lineNumbers: true,
                              readOnly: true
                        });
                  });
            </script>
      </div>
<?php
endif;
?>