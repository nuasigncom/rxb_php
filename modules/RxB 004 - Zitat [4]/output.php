<?php

$sMedia = '';
if('REX_MEDIA[1]' != '')
{
	$sImageType = 'picture-default';
	$sImageFile = 'REX_MEDIA[1]';

	$sMedia = 'index.php?rex_media_type='.$sImageType.'&rex_media_file='.$sImageFile;
}

$sParams = '';
if('REX_VALUE[3]' != '')
{
	$sParams .= ' data-parallax="true"';
}

?>

<div class="mod mod-cite"<?php echo $sParams;?>>
	<div class="cite" style="background-image: url('<?php echo $sMedia; ?>')">
		<div class="layer"></div>
		<div class="container">
			<div class="row">
				<div class="text typo-cite col-xs-12 col-md-8 col-md-offset-2">
					<blockquote>
						<cite>REX_VALUE[id=1 output="html"]</cite><br>
						<div class="author">REX_VALUE[id=2]</div>
					</blockquote>
				</div>
			</div>
		</div>
	</div>
</div>