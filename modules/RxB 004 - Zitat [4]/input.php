<?php

$mform = new mform();

$mform->addFieldset('Einstellungen');
$mform->addRadioField(3, array('0'=>'Deaktiviert','1'=>'Aktiviert'), array('label'=>'Parallax-Effekt'));

$mform->addFieldset('Inhalt');
$mform->addTextField(1,array('label'=>'Zitat'));
$mform->addTextField(2,array('label'=>'Autor'));
$mform->addMediaField(1,array('label'=>'Bild'));

echo $mform->show();

?>