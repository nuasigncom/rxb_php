<?php

$animate_json = '{

  "attention_seekers": [
    "bounce",
    "flash",
    "pulse",
    "rubberBand",
    "shake",
    "headShake",
    "swing",
    "tada",
    "wobble",
    "jello"
  ],

  "bouncing_entrances": [
    "bounceIn",
    "bounceInDown",
    "bounceInLeft",
    "bounceInRight",
    "bounceInUp"
  ],

  "bouncing_exits": [
    "bounceOut",
    "bounceOutDown",
    "bounceOutLeft",
    "bounceOutRight",
    "bounceOutUp"
  ],

  "fading_entrances": [
    "fadeIn",
    "fadeInDown",
    "fadeInDownBig",
    "fadeInLeft",
    "fadeInLeftBig",
    "fadeInRight",
    "fadeInRightBig",
    "fadeInUp",
    "fadeInUpBig"
  ],

  "fading_exits": [
    "fadeOut",
    "fadeOutDown",
    "fadeOutDownBig",
    "fadeOutLeft",
    "fadeOutLeftBig",
    "fadeOutRight",
    "fadeOutRightBig",
    "fadeOutUp",
    "fadeOutUpBig"
  ],

  "flippers": [
    "flip",
    "flipInX",
    "flipInY",
    "flipOutX",
    "flipOutY"
  ],

  "lightspeed": [
    "lightSpeedIn",
    "lightSpeedOut"
  ],

  "rotating_entrances": [
    "rotateIn",
    "rotateInDownLeft",
    "rotateInDownRight",
    "rotateInUpLeft",
    "rotateInUpRight"
  ],

  "rotating_exits": [
    "rotateOut",
    "rotateOutDownLeft",
    "rotateOutDownRight",
    "rotateOutUpLeft",
    "rotateOutUpRight"
  ],

  "specials": [
    "hinge",
    "jackInTheBox",
    "rollIn",
    "rollOut"
  ],

  "zooming_entrances": [
    "zoomIn",
    "zoomInDown",
    "zoomInLeft",
    "zoomInRight",
    "zoomInUp"
  ],

  "zooming_exits": [
    "zoomOut",
    "zoomOutDown",
    "zoomOutLeft",
    "zoomOutRight",
    "zoomOutUp"
  ],

  "sliding_entrances": [
    "slideInDown",
    "slideInLeft",
    "slideInRight",
    "slideInUp"
  ],

  "sliding_exits": [
    "slideOutDown",
    "slideOutLeft",
    "slideOutRight",
    "slideOutUp"
  ]
}';
$animate_json= (json_decode($animate_json, true));
$animate_json= array_reduce($animate_json, 'array_merge', array());
$animate_json= array_combine(array_values($animate_json),array_values($animate_json));
array_unshift($animate_json, "Keine");
// dump($animate_json); 

$mformblock = new mform();

$mformblock->addFieldset('Media');
$mformblock->addMediaField(1,array('label'=>'Picture','preview'=>'true'));
$mformblock->addTextField("1.0.video", array('label'=>'Video-URL'));
$mformblock->addTextAreaField("1.0.title", array('label'=>'Text','class'=>'redactorEditor2-full'));
$mformblock->addSelectField("1.0.animation", $animate_json, array('label'=>'Text-Animation'));
$mformblock->addSelectField("1.0.has_zoom", array(''=>'Bilder zoomen nicht',1=>'Bilder Zoomen'), array('label'=>'Zoom'));
$mformblock->addSelectField("1.0.has_overlay", array(''=>'Bildfläche wird nicht abgedunkelt',1=>'Bildfläche wird abgedunkelt'), array('label'=>'Abdunklung'));
$mformblock->addLinkField(1, array('label'=>'Link'));

$mform = new mform();

$mform->addFieldset('Einstellungen');
$mform->addRadioField("2.is_fullsize", array(''=>'Bilder werden normal Breit angezeigt',1=>'Bilder werden auf voller Breite angezeigt'), array('label'=>'Volle Breite'));
$mform->addHtml(MBlock::show(1, $mformblock->show()));

echo $mform->show();


?>