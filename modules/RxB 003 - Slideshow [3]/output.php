<?php

$i = 0;

$aValues = rex_var::toArray("REX_VALUE[1]");
$aSettings = rex_var::toArray("REX_VALUE[2]");

foreach($aValues as $oValues)
{
	$sMedia = '';
	if($oValues['REX_MEDIA_1']!='')
	{
		$sImageType = 'bp_stageslider';
		$sImageFile = $oValues['REX_MEDIA_1'];
	
		#$sMedia = '<img src="index.php?rex_media_type='.$sImageType.'&rex_media_file='.$sImageFile.'" srcset="rex_media_type='.$sImageType.'" alt="">';
		/*
    srcset="index.php?rex_media_type='.$sImageType.'__400&rex_media_file='.$sImageFile.' 480w
            index.php?rex_media_type='.$sImageType.'__700&rex_media_file='.$sImageFile.' 768w
            index.php?rex_media_type='.$sImageType.'__800&rex_media_file='.$sImageFile.' 960w
            */
		$sMedia = '<img src="index.php?rex_media_type='.$sImageType.'&rex_media_file='.$sImageFile.'"
    ">';
	}
	else if($oValues['video']!='')
	{
		$sMedia = '<iframe id="video-'.$i.'" src="https://player.vimeo.com/video/'.$oValues['video'].'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
	}

	$sTitle = '';
	if($oValues['title']!='')
	{
		$sTitle = '<div class="title-wrapper" data-animation="'.$oValues['animation'].'" data-delay="0.5s"><div class="title typo-slider">'.$oValues['title'].'</div></div>';
	}

	$sUrl = '';
	if($oValues['REX_LINK_1']!='')
	{
		$sUrl = rex_getUrl($oValues['REX_LINK_1']);
	}

	$sClass = '';
	if($oValues['has_zoom'])
	{
		$sClass .= ' has_zoom';
	}
	else if($oValues['has_overlay'])
	{
		$sClass .= ' has_overlay';
	}
	
	$sOut .= '
		<div class="slide'.$sClass.'">
			<div class="overlay"></div>
			'.( $sUrl != '' ? '<a href="'.$sUrl.'">' : '' ).'
				<div class="media">'.$sMedia.'</div>
				'.$sTitle.'
			'.( $sUrl != '' ? '</a>' : '' ).'
		</div>
	';
	
	$i++;
}

if(!$aSettings['is_fullsize'])
{
	$sOut = '
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="slider slider_in_grid">
						'.$sOut.'
					</div>
				</div>
			</div>
		</div>
	';
}
else
{
	$sOut = '
		<div class="slider slider_fullsize">
			'.$sOut.'
		</div>
	';
}

?>

<div class="mod mod-slider">
	<?php echo $sOut; ?>
</div>