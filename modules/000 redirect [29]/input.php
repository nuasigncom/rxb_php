<?php 
/*
 * Modul Redirect
 *
 * author	: STEFAN BUKACEK
 * version	: 1.5, 1.6 fuer R5, 1.7 mit mform
 *
 
?>
	
<fieldset>
<strong>Artikel</strong><br />
REX_LINK[id=1 widget=1]
 
<strong>Redirect-Attribute-Array</strong> (separiert mit Doppelpunkt: <strong>KEY:VALUE</strong> - Beispiel foo:bar)<br />
<textarea name="VALUE[1]" value="REX_VALUE[1]" cols="50" rows="5" class="inp100">REX_VALUE[1]</textarea> 
<br />
</fieldset>

<hr />

<fieldset>
<strong>Externe Webseite</strong><br />
<textarea name="VALUE[2]" value="REX_VALUE[2]" cols="50" rows="5" class="inp100">REX_VALUE[2]</textarea> 
<br />
</fieldset>


<fieldset>
<strong>Weiterleitung, falls Benutzergruppe mit folgender ID</strong><br />
<input name="VALUE[3]" value="REX_VALUE[3]" type="text" class="inp100">
<br />
</fieldset>
<?php
* */
// init mform
$mform = new MForm();

// fieldset
$mform->addFieldset('Interne Weiterleitung');
$mform->addLinkField(1,array('label'=>'Ziel-Artikel'));
$mform->addTextAreaField(1, array('label'=>'<strong>Redirect-Attribute-Array</strong> (separiert mit Doppelpunkt: <strong>KEY:VALUE</strong> - Beispiel foo:bar)'));


// fieldset
$mform->addFieldset('Externe Weiterleitung');
$mform->addTextField(2, array('label'=>'Externe Webseite')); // use string for x.0 json values

// fieldset
$mform->addFieldset('Benutzergruppen');
$mform->addTextField(3, array('label'=>'Für Redaxo User-Einschränkungen')); // use string for x.0 json values


// parse form
echo $mform->show();

?>