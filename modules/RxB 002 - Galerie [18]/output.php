<?php

$sImageType = 'picture_gallery';
$sImageZoomType = 'picture_gallery_zoom';


$aBlocks = rex_var::toArray("REX_VALUE[1]");
$aSettings = rex_var::toArray("REX_VALUE[2]");
$aText = rex_var::toArray("REX_VALUE[3]");
$aPictures = explode(',', "REX_MEDIALIST[2]");

$sOut = '';
$sText = '';

if ($aText['header'] != "") {
    $sText .= '<h3 class="gallery-header">'.$aText['header'].'</h3>';
}
if ($aText['teaser'] != "") {
    $sText .= '<p class="gallery-teaser">'.$aText['teaser'].'</p>';
}
if ($sText != "") {
    $sText = '<div class="col-xs-12 col-sm-12 gallery-text'. ( ($aSettings['textPosition'] === 'center') ? "-centered" : "" ) .'">'.$sText.'</div>';
}


if ($aSettings['pictureFormat'] === 'slim') {
    $sImageType = 'picture-slim';
}

$sWidth = '4';
if ($aSettings['picturesPerRow'] === '1') {
    $sWidth = '12';
} elseif ($aSettings['picturesPerRow'] === '2') {
    $sWidth = '6';
} elseif ($aSettings['picturesPerRow'] === '3') {
    $sWidth = '4';
} elseif ($aSettings['picturesPerRow'] === '4') {
    $sWidth = '3';
}
#dump($aSettings);

if (!empty($aPictures)) {
    foreach ($aPictures as $oPicture) {
        $sMedia = '';
        $sTitle = '';

        if ($oPicture != '') {
            $sImageFile = $oPicture;

            $oMedia = rex_media::get($sImageFile);

            if (is_object($oMedia)) {
                if ($oMedia->getTitle() != '') {
                    $sTitle = '<div class="gallery-item-title typo-gallery">'.$oMedia->getTitle().'</div>';
                }

								$sMedia = '<a href="index.php?rex_media_type='.$sImageZoomType.'&rex_media_file='.$sImageFile.'" class="swipebox" rel="gallery-REX_SLICE_ID">';
								if (rex_addon::get('media_type_manager')->isAvailable()) {
                    $additionalAttributes = ["class" => "galheight".$aSettings['picturesPerRow']];
                    $sMedia .= rex_media_type_set_helper::getPictureTag($sImageType, $sImageFile, $additionalAttributes);
                }
								else {
                    $sMedia .= '<img loading="lazy" src="index.php?rex_media_type='.$sImageType.'&rex_media_file='.$sImageFile.'" alt="">';
                }
								$sMedia .= '</a>';

                $sOut .= '
				<div class="gallery-item col-xs-12 col-sm-'.$sWidth.' matchHeight">
					<div class="wrapper">
						<div class="gallery-item-media">'.$sMedia.'</div>
					</div>
					'.$sTitle.'
				</div>
			';
            }
        }
    }
}

if (!empty($aBlocks)) {
    foreach ($aBlocks as $oBlock) {
        if (array_filter($oBlock)) {
            $sMedia = '';
            if ($oBlock['REX_MEDIA_1'] != '') {
                $sImageFile = $oBlock['REX_MEDIA_1'];

                $oMedia = rex_media::get($sImageFile);

                if (rex_addon::get('media_type_manager')->isAvailable()) {
                    $additionalAttributes = ["class" => "galheight".$aSettings['picturesPerRow']];
                    $sMedia .= rex_media_type_set_helper::getPictureTag($sImageZoomType, $sImageFile, $additionalAttributes);
                } else {
                    $sMedia = '<a href="index.php?rex_media_type='.$sImageZoomType.'&rex_media_file='.$sImageFile.'" class="swipebox" rel="gallery-REX_SLICE_ID"><img loading="lazy" src="index.php?rex_media_type='.$sImageType.'&rex_media_file='.$sImageFile.'" alt=""></a>';
                }
            }

            $sTitle = '';
            if ($oBlock['title'] != '') {
                $sTitle = '<div class="gallery-item-title typo-gallery">'.$oBlock['title'].'</div>';
            }

            $sOut .= '
				<div class="gallery-item col-xs-12 col-sm-'.$sWidth.'">
					<div class="wrapper">
						<div class="gallery-item-media">'.$sMedia.'</div>
						'.$sTitle.'
					</div>
				</div>
			';
        }
    }
}

?>

<div class="mod mod-gallery sliceid-REX_SLICE_ID" data-text-position="<?php echo $aSettings['textPosition']; ?>" data-background="<?php echo $aSettings['bg']; ?>" data-fullwidth="<?php echo $aSettings['fullwidth']; ?>">
	<div class="container">
		<div class="gallery row">
            <?php if(($aSettings['textPosition'] === "top") OR ($aSettings['textPosition'] === "")) echo $sText; ?>
            <?php echo $sOut; ?>
            <?php if(($aSettings['textPosition'] === "bottom") OR ($aSettings['textPosition'] === "center")) echo $sText; ?>
		</div>
	</div>
</div>
