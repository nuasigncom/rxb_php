<?php

$mformblock = new mform();

$mformblock->addFieldset('Bild');
$mformblock->addMediaField(1,array('label'=>'Bild','preview'=>'true'));
$mformblock->addTextAreaField("1.1.title", array('label'=>'Beschreibungstext'));

$mform = new mform();

$mform->addFieldset('Text');
$mform->addTextField('3.header', array('label'=>'Titel fuer Galerieblock'));
$mform->addTextField('3.teaser', array('label'=>'Teaser / Zusatzinfo fuer Galerieblock'));

$mform->addFieldset('Einstellungen');
$mform->addRadioField('2.bg', array('default'=>'Transparent','highlighted'=>'Hervorgehoben'), array('label'=>'Hintergrund'));
$mform->addRadioField('2.picturesPerRow', array('1'=>'1','2'=>'2','3'=>'3','4'=>'4'), array('label'=>'Bilder pro Zeile'));
$mform->addSelectField('2.pictureFormat', array('slim'=>'schmal',/*'small'=>'geringe Höhe, unbeschnitten',*/''=>'normal'), array('label'=>'Bildformat (Hoehe)'));
$mform->addRadioField('2.textPosition', array('top'=>'Oberhalb','bottom'=>'Unterhalb','center'=>'Mittig'), array('label'=>'Textposition'));
$mform->addRadioField('2.fullwidth', array(false=>'Nein',true=>'Ja'), array('label'=>'Volle Breite'));

$mform->addFieldset('Bilder');
$mform->addMediaListField(2, array('preview'=>1, 'label'=>'Bilder'));

$mform->addFieldset('Bilder (einzeln)');
$mform->addHtml(MBlock::show(1, $mformblock->show()));

echo $mform->show();

?>