<?php
// image manager typen
$results = rex_sql::factory()->getArray("SELECT id,name,description FROM rex_media_manager_type");
$results[0] = array(""=>0,"name"=>"","description"=>"normales Bildformat - ");
foreach ($results as $result){ 
    $selectme[$result['name']] = $result['name']." - ".$result['description'];
}

//mblock item
$mformblock = new mform();

$mformblock->addFieldset('Inhalt');
$mformblock->addMediaField(1,array('label'=>'Bild','preview'=>'true'));
$mformblock->addTextField("1.1.metaheader", array('label'=>'Meta-Hader'));
$mformblock->addDescription('Kurzer Text oberhalb des Teasers, z.B. für Tags, Datum, Ressort o.ä.');
$mformblock->addTextField("1.1.title", array('label'=>'Titel'));
$mformblock->addTextField("1.1.subtitle", array('label'=>'Subtitel'));
$mformblock->addTextAreaField("1.1.text", array('label'=>'Text'));
$mformblock->addTextAreaField("1.1.text2", array('label'=>'Text 2.Spalte (optional)'));
$mformblock->addLinkField(1, array('label'=>'Link'));
$mformblock->addTextField("1.1.linktext", array('label'=>'Link-Text'));


//settings
$mform = new mform();

//$mform->addCheckboxField('2.source',array(1=>'Experten'),array('label'=>'Expertenmodus'));

$mform->addFieldset('Quelle');
$mform->addRadioField('2.source', array('auto'=>'Automatisch aus Artikeln',''=>'Manuell'), array('label'=>'Wie sollen die Teaser erstellt werden?'));

$mform->addFieldset('Manuelle Teaser erstellen');
$mform->addHTML(MBlock::show(1, $mformblock->show()));

$mform->addFieldset('Einstellungen für automatische Teaser');
$mform->addDescription('Normalerweise werden die Unterkategorien des aktuellen Artikels gelistet.');
$mform->addLinkField(3, array('label'=>'Falls automatisch: Start-Kategorie','note'=>''));
$mform->addDescription('Der Startartikel, der hier ausgewählt wird, bestimmt die Kategorie, für die die Artikel aufgelistet werden. Wird der Link gelöscht, werden die Unterkategorien von dieser Kategorie gelistet.');


$mform->addFieldset('Einstellungen für den Teaserblock');
$mform->addSelectField('2.bg', array('default'=>'Transparent','highlighted'=>'Hervorgehoben'), array('label'=>'Hintergrund'));
$mform->addSelectField('2.distance', array(''=>'kein','mb'=>'Unterhalb','mt'=>'Oberhalb','mtb'=>'Ober- und Unterhalb'), array('label'=>'Abstand (für die einzelnen Teaser)'));
$mform->addSelectField('2.picturesPerRow', array('1'=>'1','2'=>'2','3'=>'3','4'=>'4'), array('label'=>'Teaser-Bilder pro Zeile'));
$mform->addSelectField('2.fullwidth', array(false=>'Nein',true=>'Ja'), array('label'=>'Volle Breite'));


$mform->addFieldset('Einstellungen für die Text-Positionen');
$mform->addSelectField('2.titlePosition', array('bottom'=>'Unterhalb','top'=>'Oberhalb','center'=>'Mittig'), array('label'=>'Titel und Subtitel Position'));
$mform->addSelectField('2.textPosition', array(''=>'Teasertext ausgeblendet','bottom'=>'Unterhalb','top'=>'Oberhalb','center'=>'Mittig'), array('label'=>'Teaser-Text Position'));



$mform->addFieldset('Experten-Einstellungen');
$mform->addDescription('Nähere Angaben zu diesem Punkt erhalten Sie von Ihrem Webentwickler.');
$mform->addMediaField(1,array('label'=>'Teaserbild für übergeordnete Kategorie','preview'=>'true'));
$mform->addDescription('Bild, das in der übergeordneten Übersicht für diese Kategorie angezeigt werden soll, falls in diesem Artikel kein anderes Modul liegt und die Teaser automatisch erstellt werden.');
$mform->addTextField("2.cssClass", array('label'=>'CSS Klasse','class'=>'expert')); 
$mform->addSelectField("2.imageType", $selectme, array('label'=>'Optional: Bildformat / Mediatyp ID','class'=>'expert'));
//Nähere Angaben zu diesem Punkt erhalten Sie von Ihrem Webentwickler.





echo $mform->show();

?>

<script>

$(document).ready(function(){

	$('.form-horizontal:nth-child(1) .form-group .radio:nth-child(1) input').click(function()
	{
		$('.form-horizontal:nth-child(2)').slideUp();
	});
	$('.form-horizontal:nth-child(1) .form-group .radio:nth-child(2) input').click(function()
	{
		$('.form-horizontal:nth-child(2)').slideDown();
	});
    
    //.form-horizontal > fieldset:nth-child(1) > div > div.col-sm-10 > div:nth-child(1) > label
	if($('fieldset.form-horizontal:nth-child(1) .form-group .radio:nth-child(1) input').attr('checked') == 'checked') 
        {$('fieldset.form-horizontal:nth-child(2)').hide();}
	//if($('.form-horizontal:nth-child(2) .form-group .select:nth-child(2) input').attr('checked') == 'checked') $('.form-horizontal:nth-child(2)').hide();



});

</script>
<style>
    .mform .form-group .control-label {
        padding: 7px;
    }
</style>

