<?php

$i = 0;
$aSettings = rex_var::toArray("REX_VALUE[2]");
//dump(rex_var::toArray("REX_LINK[3]"),"REX_LINK[3]");
$module = rex_config::get('gloebals', 'rex-bolierplate')['settings']['modules'];

if( ($aSettings['source'] === 'auto') OR ($aSettings['source'] === 'rexlink'))
{
	// $aValues
	$aArticles = rex_category::getCurrent()->getChildren(true);
	if("REX_LINK[3]" != "")
	{
		$aArticles = rex_category::get("REX_LINK[3]")->getChildren(true);
	}

	foreach($aArticles as $oArticle)
	{
		$aValues[$i]['REX_LINK_1'] = $oArticle->getId();
		$aValues[$i]['title'] = $oArticle->getName();
		$aValues[$i]['REX_MEDIA_1'] = "rex_bp_cat_default.png";

		$aSlices = rex_article_slice::getSlicesForArticleOfType($oArticle->getId(),REX_SLICE_ID,true); // Modul Teaser itself
		if(!empty($aSlices))
		{
			if($aSlices[0]->getMediaList(2) != '')
			{
				$aTemp = explode(',',$aSlices[0]->getMediaList(2)); // automatisch
				$aValues[$i]['REX_MEDIA_1'] = $aTemp[0]; // manuell
			}
			else
			{
				$aTemp = rex_var::toArray($aSlices[0]->getValue(1));
				$aValues[$i]['REX_MEDIA_1'] = $aTemp[0]['REX_MEDIA_1'];
			}
		}
		$aSlices = rex_article_slice::getSlicesForArticleOfType($oArticle->getId(),$module['gallery'],true); // Modul Gallerie
		if(!empty($aSlices))
		{
			if($aSlices[0]->getMediaList(2) != '')
			{
				$aTemp = explode(',',$aSlices[0]->getMediaList(2)); // automatisch
				$aValues[$i]['REX_MEDIA_1'] = $aTemp[0]; // manuell
			}
			else
			{
				$aTemp = rex_var::toArray($aSlices[0]->getValue(1));
				$aValues[$i]['REX_MEDIA_1'] = $aTemp[0]['REX_MEDIA_1'];
			}
		}

		$aSlices = rex_article_slice::getSlicesForArticleOfType($oArticle->getId(),$module['slideshow'],true); // Modul Slideshow
		if(!empty($aSlices))
		{
			$aTemp = rex_var::toArray($aSlices[0]->getValue(1));
			$aValues[$i]['REX_MEDIA_1'] = $aTemp[0]['REX_MEDIA_1'];
		}

		$aSlices = rex_article_slice::getSlicesForArticleOfType($oArticle->getId(),$module['image'],true); // Modul Bild
		if(!empty($aSlices))
		{
			$aValues[$i]['REX_MEDIA_1'] = $aSlices[0]->getMedia(1);
		}

		$aSlices = rex_article_slice::getSlicesForArticleOfType($oArticle->getId(),$module['text'],true); // Modul Fließtext
		if(!empty($aSlices))
		{
			$aValues[$i]['text'] = strlen($aSlices[0]->getValue(1)) > 75 ? strip_tags(mb_substr($aSlices[0]->getValue(1),0,75).'...') : strip_tags($aSlices[0]->getValue(1));
		}

		$i++;
	}
}
else
{
	$aValues = rex_var::toArray("REX_VALUE[1]");
}

#dump($aValues);
//kill notices
    if (!isset($oValues['metaheader']))      {$oValues['metaheader'] = "";}
    if (!isset($oValues['title']))      {$oValues['title'] = "";}
    if (!isset($oValues['subtitle']))      {$oValues['subtitle'] = "";}
    if (!isset($oValues['text2']))      {$oValues['text2'] = "";}
    if (!isset($oValues['text']))       {$oValues['text'] = "";}
    if (!isset($oValues['link-text']))  {$oValues['link-text'] = "";} //deprecated; link-text wurde mit mform nicht sauber gespeichert wegen bindestrick
    if (!isset($oValues['linktext']))  {$oValues['linktext'] = "";}
    if (!isset($oValues['REX_LINK_1'])) {$oValues['REX_LINK_1'] = "";}
    if (!isset($oValues['REX_MEDIA_1']))        {$oValues['REX_MEDIA_1'] = "";}
    if (!isset($aSettings['picturesPerRow']))   {$aSettings['picturesPerRow'] = "";}
    if (!isset($aSettings['textPosition']))     {$aSettings['textPosition'] = "";}
    if (!isset($aSettings['titlePosition']))    {$aSettings['titlePosition'] = "";}
    if (!isset($aSettings['bg']))               {$aSettings['bg'] = "";}
    if (!isset($aSettings['distance']))         {$aSettings['distance'] = "";}
    if (!isset($aSettings['imageType']))        {$aSettings['imageType'] = "";}


$sOut = '';
foreach($aValues as $oValues)
{
    #dump($oValues);
    if( isset($oValues['REX_LINK_1']) ) $rex_link_1 = $oValues['REX_LINK_1'];

	$sMedia = '';
	if( isset($oValues['REX_MEDIA_1']) )
	{
		$sImageType = 'picture-teaser';
        if($aSettings['imageType'] != "") $sImageType = $aSettings['imageType'];
		$sImageFile = $oValues['REX_MEDIA_1'];

     if( $rex_link_1 != "" ) //&& $oValues['link-text'] != ''
     {$sMedia .= '<a href="'.rex_getUrl($oValues['REX_LINK_1'],rex_clang::getCurrentId()).'">';}

     //$sMedia .= '<img src="index.php?rex_media_type='.$sImageType.'&rex_media_file='.$sImageFile.'" alt="" class="img-responsive"/>';
     //$sMedia .= '<img loading=lazy src="'.rex_media_manager::getUrl($sImageType, $sImageFile, $timestamp = null).'" alt="" class="img-responsive" />';
        $sMedia .= '<img load="lazy" data-src="'.rex_media_manager::getUrl($sImageType, $sImageFile, $timestamp = null).'" alt="" class="img-responsive lazyload" />';

     if( $rex_link_1 != "" ) //&& $oValues['link-text'] != ''
     {$sMedia .= '</a>';}
 }




// create item
	$sTitle = '';
	if(isset($oValues['title']))
	{
		$sTitle = '<div class="teaser-item-title typo-gallery">'.$oValues['title'].'</div>';
	}

	$sSubtitle = '';
	if(isset($oValues['subtitle']))
	{
		$sSubtitle = '<div class="teaser-item-subtitle">'.$oValues['subtitle'].'</div>';
	}

	$sMetaheader = '';
	if(isset($oValues['metaheader']))
	{
		$sMetaheader = '<div class="teaser-item-metaheader text-muted">'.$oValues['metaheader'].'</div>';
	}

	$sText = '';
	if(isset($oValues['text']))
	{   
        $sText = markitup::parseOutput ('textile', strip_tags($oValues['text']));
		$sText = '<div class="teaser-item-text typo-gallery">'.$sText.'</div>';
	}
    if(isset($oValues['text2']))
	{
        $sText = markitup::parseOutput ('textile', strip_tags($oValues['text']));
        $sText2 = markitup::parseOutput ('textile', strip_tags($oValues['text2']));
		$sText = '<div class="row teaser-item-text typo-gallery"><div class="col-sm-6">'.$sText.'</div><div class="col-sm-6">'.$sText2.'</div></div>';
	}

	$sButton = '';
	$linktext = '';
    
    if (isset($oValues['link-text'])) $linktext = $oValues['link-text']; //deprecated
    if (isset($oValues['linktext'])) $linktext = $oValues['linktext']; 

    if(
            (isset($oValues['REX_LINK_1']) && $linktext != "")
    ) 
	{
		$sButton = '<div class="teaser-item-link typo-teaser"><a class="btn btn-primary" href="'.rex_getUrl($oValues['REX_LINK_1'],rex_clang::getCurrentId()).'">'.$linktext.'</a></div>';
	}



	$sWidth = '4';
    if(isset($aSettings['picturesPerRow'])){
        if($aSettings['picturesPerRow'] === '1')
        {
            $sWidth = '12';
        }
        else if($aSettings['picturesPerRow'] === '2')
        {
            $sWidth = '6';
        }
        else if($aSettings['picturesPerRow'] === '3')
        {
            $sWidth = '4';
        }
        else if($aSettings['picturesPerRow'] === '4')
        {
            $sWidth = '3';
        }
    }

    $sOut .= '
		<div class="teaser-item col-xs-12 col-sm-'.$sWidth.' teaserid-'.$i++.' '.$aSettings['distance'].'">
			<div class="wrapper">
                    '.
                        ( ($aSettings['titlePosition'] == 'top' ) ? '<div class="teaser-item-metaheader small">'.$sMetaheader.'</div>' : '' )
                    .'
                    '.
                        ( ($aSettings['titlePosition'] == 'top' ) ? '<div class="teaser-item-title h3">'.$sTitle.'</div>' : '' )
                    .
                        ( ($aSettings['titlePosition'] == 'top' ) ? '<div class="teaser-item-subtitle teasertext">'.$sSubtitle.'</div>' : '' )
                    .
                        ( ($aSettings['textPosition'] == 'top' ) ? '<div class="teaser-item-text text">'.$sText.'</div>' : '' )
                    .'
				<div class="teaser-item-media">
					'.
                        $sMedia
                    .'
				</div>
				<div class="teaser-item-data teaser-item-data-after">
					'.
                        ( ($aSettings['titlePosition'] != 'top' ) ? '<div class="teaser-item-title h3">'.$sTitle.'</div>' : '' )
                    .'
					'.
                        ( ($aSettings['titlePosition'] != 'top' ) ? '<div class="teaser-item-subtitle teasertext">'.$sSubtitle.'</div>' : '' )
                    .'
                    '.
                        ( ($aSettings['textPosition'] == 'bottom' ) ? '<div class="teaser-item-text text">'.$sText.'</div>' : '' )
                    .'
				</div>
				'.$sButton.'
			</div>
		</div>
	';
}
#dump($aSettings);
?>

<section class="mod mod-teaser sliceid-REX_SLICE_ID <?php if($aSettings['bg'] == "highlighted") {echo "grey highlighted";} ?>" data-text-position="<?php echo $aSettings['textPosition']; ?>" data-background="<?php echo $aSettings['bg']; ?>" data-fullwidth="<?php echo $aSettings['fullwidth']; ?>">
	<div class="container">
        <div class="teaser row">
            <?php echo $sOut; ?>
        </div>
    </div>
</section>