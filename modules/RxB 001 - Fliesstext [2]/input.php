<?php

$mform = new mform();

$mform->addFieldset('Inhalt');
$mform->addTextAreaField(1,array('label'=>'Freitext','class'=>'redactorEditor2-full'));

$mform->addFieldset('Einstellungen');
$mform->addSelectField(2, array('default'=>'Transparent','highlighted'=>'Hervorgehoben'), array('label'=>'Hintergrund'));

echo $mform->show();

?>